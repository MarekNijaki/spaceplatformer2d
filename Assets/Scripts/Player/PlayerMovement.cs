using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	#region Serialized fields

	/// <summary>
	///   Character controller.
	/// </summary>
	[SerializeField]
	[Tooltip("Character controller")]
	private CharacterController2D _characterController;
	
	/// <summary>
	///   Character animator.
	/// </summary>
	[SerializeField]
	[Tooltip("Character animator")]
	private Animator _animator;

	/// <summary>
	///   Character run speed.
	/// </summary>
	[SerializeField]
	[Tooltip("Character run speed")]
	private float _runSpeed = 60.0F;

	#endregion

	#region Private fields
	
	/// <summary>
	///   Horizontal speed of character.
	/// </summary>
	private float _horizontalSpeed;

	/// <summary>
	///   Name of animator property for 'Speed'.
	/// </summary>
	private static readonly int _animatorNameForSpeedProperty = Animator.StringToHash("Speed");
	
	/// <summary>
	///   Name of animator property for 'IsJumping'.
	/// </summary>
	private static readonly int _animatorNameForIsJumpingProperty = Animator.StringToHash("IsJumping");
	
	/// <summary>
	///   Name of animator property for 'IsCrouching'.
	/// </summary>
	private static readonly int _animatorNameForIsCrouchingProperty = Animator.StringToHash("IsCrouching");
	
	/// <summary>
	///   Flag, informing if character is jumping.
	/// </summary>
	private bool _isJumping;
	
	/// <summary>
	///   Flag, informing if character is crouching.
	/// </summary>
	private bool _isCrouching;

	#endregion

	#region Public methods

	/// <summary>
	///   Function to run after character has landed on the ground.
	/// </summary>
	public void OnLanded()
	{
		_animator.SetBool(_animatorNameForIsJumpingProperty, false);
	}

	/// <summary>
	///   Function to run after character enter crouch mode or exit crouch mode.
	/// </summary>
	public void OnCrouchStateChanged(bool isCrouching)
	{
		_animator.SetBool(_animatorNameForIsCrouchingProperty, isCrouching);
	}

	#endregion
	
	#region Private methods

	/// <summary>
	///   Update is called once per frame.
	/// </summary>
	private void Update()
	{
		// Horizontal movement.
		_horizontalSpeed = Input.GetAxisRaw("Horizontal") * _runSpeed;
		_animator.SetFloat(_animatorNameForSpeedProperty, Mathf.Abs(_horizontalSpeed));

		// Jump.
		if(Input.GetButtonDown("Jump"))
		{
			_isJumping = true;
			_animator.SetBool(_animatorNameForIsJumpingProperty, true);
		}

		// Crouch.
		if (Input.GetButtonDown("Crouch"))
		{
			_isCrouching = true;
		} 
		else if (Input.GetButtonUp("Crouch"))
		{
			_isCrouching = false;
		}
	}

	/// <summary>
	///   Fixed update.
	/// </summary>
	private void FixedUpdate()
	{
		_characterController.Move(_horizontalSpeed * Time.fixedDeltaTime, _isCrouching, _isJumping);
		_isJumping = false;
	}

	#endregion
}