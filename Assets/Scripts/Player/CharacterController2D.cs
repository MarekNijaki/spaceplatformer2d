using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{
	#region Public fields

	/// <summary>
	///   Event to rise when player landed on the ground.
	/// </summary>
	[Header("Events")]
	[Tooltip("Event to rise when player landed on the ground")]
	public UnityEvent OnLanded;

	/// <summary>
	///   Event to rise when player enter crouch mode or exit crouch mode.
	/// </summary>
	[Tooltip("Event to rise when player enter crouch mode or exit crouch mode")]
	public UnityEvent<bool> OnCrouchStateChanged;

	#endregion
	
	#region Serialized fields

	/// <summary>
	///   Amount of force added when the player jumps.
	/// </summary>
	[Header("Movement settings")]
	[SerializeField]
	[Tooltip("Amount of force added when the player jumps")]
	private float _jumpForce = 400.0F; 
	
	/// <summary>
	///   Crouch speed factor is a percentage value of normal movement speed.
	///   Used to calculate crouch speed.
	///   1 = 100%.
	///   0 = 0%.
	/// </summary>
	[Range(0, 1)]
	[SerializeField]
	[Tooltip("Crouch speed factor is a percentage value of normal movement speed. " + 
	         "Used to calculate crouch speed.  1 = 100%. 0 = 0%.")]
	private float _crouchSpeedFactor = 0.36F;
	
	/// <summary>
	///   Factor applied to smooth out the movement.
	/// </summary>
	[Range(0, .3f)]
	[SerializeField]
	[Tooltip("Factor applied to smooth out the movement")]
	private float _movementSmoothFactor = 0.05F; 
	
	/// <summary>
	///   Flag, informing if player can steer while jumping.
	/// </summary>
	[SerializeField]
	[Tooltip("Flag, informing if player can steer while jumping")]
	private bool _isAirControlAllowed;
	
	/// <summary>
	///   A mask determining what is ground to the character.
	/// </summary>
	[Header("Movement checks settings")]
	[SerializeField]
	[Tooltip("A mask determining what is ground to the character")]
	private LayerMask _groundMask;
	
	/// <summary>
	///   A position marking where to check if the player is grounded.
	/// </summary>
	[SerializeField]
	[Tooltip("A position marking where to check if the player is grounded")]
	private Transform _groundCheck;
	
	/// <summary>
	///   Radius of the overlap circle to determine if the player is grounded.
	/// </summary>
	[SerializeField]
	[Tooltip("Radius of the overlap circle to determine if the player is grounded")]
	public float _groundCheckRadius = 0.2f;
	
	/// <summary>
	///   A position marking where to check for ceilings.
	/// </summary>
	[SerializeField]
	[Tooltip("A position marking where to check for ceilings")]
	private Transform _ceilingCheck;
	
	/// <summary>
	///   Radius of the overlap circle to determine if the player can stand up.
	/// </summary>
	[SerializeField]
	[Tooltip("Radius of the overlap circle to determine if the player can stand up")]
	private float _ceilingCheckRadius = 0.2F;
	
	/// <summary>
	///   A collider that will be disabled when crouching.
	/// </summary>
	[SerializeField]
	[Tooltip("A collider that will be disabled when crouching")]
	private Collider2D _colliderToDisableWhenCrouching;

	#endregion
	
	#region Private fields

	/// <summary>
	///   Flag, informing if character is facing right.
	/// </summary>
	private bool _isFacingRight = true; 
	
	/// <summary>
	///   Flag, informing if character is grounded.
	/// </summary>
	private bool _isGrounded;

	/// <summary>
	///   Temporary collection, to store overlapped colliders.
	/// </summary>
	private readonly Collider2D[] _tmmOverlappedColliders = new Collider2D[1];
	
	/// <summary>
	///   Rigidbody of character.
	/// </summary>
	private Rigidbody2D _rigidbody;
	
	/// <summary>
	///   Temporary velocity.
	/// </summary>
	private Vector3 _tmpVelocity = Vector3.zero;
	
	/// <summary>
	///   Flag, informing if character was crouching.
	/// </summary>
	private bool _wasCrouching;

	#endregion
	
	#region Public fields
	
	/// <summary>
	///   Move character.
	/// </summary>
	/// <param name="moveSpeed">Move speed of character</param>
	/// <param name="shouldCrouch">Flag, if character should crouch</param>
	/// <param name="shouldJump">Flag, if character should jump</param>
	public void Move(float moveSpeed, bool shouldCrouch, bool shouldJump)
	{
		// If character should not crouch, check if character can stand up.
		if(!shouldCrouch)
		{
			// If the character has a ceiling preventing them from standing up, keep them crouching.
			if(Physics2D.OverlapCircle(_ceilingCheck.position, _ceilingCheckRadius, _groundMask))
			{
				shouldCrouch = true;
			}
		}

		// Only control the player if grounded or airControl is turned on.
		if(_isGrounded || _isAirControlAllowed)
		{
			// If crouching
			if(shouldCrouch)
			{
				if(!_wasCrouching)
				{
					_wasCrouching = true;
					OnCrouchStateChanged.Invoke(true);
				}

				// Reduce the speed by the crouch speed factor.
				moveSpeed *= _crouchSpeedFactor;

				// Disable one of the colliders when crouching.
				if(_colliderToDisableWhenCrouching != null)
				{
					_colliderToDisableWhenCrouching.enabled = false;
				}
			}
			else
			{
				// Enable the collider when not crouching.
				if(_colliderToDisableWhenCrouching != null)
				{
					_colliderToDisableWhenCrouching.enabled = true;
				}

				if(_wasCrouching)
				{
					_wasCrouching = false;
					OnCrouchStateChanged.Invoke(false);
				}
			}

			// Move the character by finding the target velocity.
			Vector3 targetVelocity = new Vector2(moveSpeed * 10f, _rigidbody.velocity.y);
			// And then smoothing it out and applying it to the character.
			_rigidbody.velocity = Vector3.SmoothDamp(_rigidbody.velocity, targetVelocity, ref _tmpVelocity, _movementSmoothFactor);

			// If the input is moving the player right and the player is facing left...
			if(moveSpeed > 0 && !_isFacingRight)
			{
				Flip();
			}
			// Otherwise if the input is moving the player left and the player is facing right...
			else if(moveSpeed < 0 && _isFacingRight)
			{
				Flip();
			}
		}
		
		// If the player should jump.
		if(_isGrounded && shouldJump)
		{
			// Add a vertical force to the player.
			_isGrounded = false;
			_rigidbody.AddForce(new Vector2(0f, _jumpForce));
		}
	}
	
	#endregion
	
	#region Private methods

	/// <summary>
	///   Awake.
	/// </summary>
	private void Awake()
	{
		_rigidbody = GetComponent<Rigidbody2D>();

		if(OnLanded == null)
		{
			OnLanded = new UnityEvent();
		}

		if(OnCrouchStateChanged == null)
		{
			OnCrouchStateChanged = new UnityEvent<bool>();
		}
	}

	/// <summary>
	///   Fixed update.
	/// </summary>
	private void FixedUpdate()
	{
		CheckIfCharacterIsGrounded();
	}

	/// <summary>
	///   Check if character is on the ground.
	///   Character is on the ground if a circle cast to the ground hits anything designated as ground.
	/// </summary>
	private void CheckIfCharacterIsGrounded()
	{
		bool wasGrounded = _isGrounded;
		_isGrounded = false;

		GetOverlappedGroundColliders();
		if(IsNoOverlappedObjects() || IsOverlappedObjectIsCharacter())
		{
			return;
		}
		
		_isGrounded = true;
		if(!wasGrounded)
		{
			OnLanded.Invoke();
		}
	}

	/// <summary>
	///   Get ground colliders that overlap with character.
	/// </summary>
	private void GetOverlappedGroundColliders()
	{
		_tmmOverlappedColliders.SetValue(null, 0);
		Physics2D.OverlapCircleNonAlloc(_groundCheck.position, _groundCheckRadius, _tmmOverlappedColliders, _groundMask);
	}

	/// <summary>
	///   Check if there is any ground object that overlapped with character.
	/// </summary>
	/// <returns>Return flag, if there is any ground object that overlapped with character</returns>
	private bool IsNoOverlappedObjects()
	{
		return (_tmmOverlappedColliders[0] == null);
	}
	
	/// <summary>
	///   Check if overlapped ground object is a main character.
	/// </summary>
	/// <returns>Return flag, if overlapped ground object is a main character.</returns>
	private bool IsOverlappedObjectIsCharacter()
	{
		return (_tmmOverlappedColliders[0].gameObject == gameObject);
	}
	
	/// <summary>
	///   Flip character graphics.
	/// </summary>
	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		_isFacingRight = !_isFacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	#endregion
}